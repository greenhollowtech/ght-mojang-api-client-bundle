<?php

namespace GHT\MojangApiClientBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Defines configuration settings.
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ght_mojang_api_client');

        $rootNode
            ->children()
                ->arrayNode('methods')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('domain')->isRequired()->end()
                            ->booleanNode('oauth')->defaultFalse()->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
