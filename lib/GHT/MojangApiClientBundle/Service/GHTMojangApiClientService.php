<?php

namespace GHT\MojangApiClientBundle\Service;

use GHT\MojangApiClient\Authentication\Credentials;
use GHT\MojangApiClient\Exception\MojangApiException;
use GHT\MojangApiClient\GHTMojangApiClient;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException as DependencyInjectionException;
use Symfony\Component\Validator\Constraints\Url as UrlConstraint;
use Symfony\Component\Validator\Validation;

/**
 * Handles Mojang API authentication and requests.
 */
class GHTMojangApiClientService
{
    /**
     * @var array<\GHT\MojangApiClient\GHTMojangApiClient>
     */
    protected $clients;

    /**
     * @var \GHT\MojangApiClient\Authentication\Credentials
     */
    protected $credentials;

    /**
     * @var string
     */
    protected $lastError;

    /**
     * @var array
     */
    protected $methodMap;

    /**
     * The constructor.
     *
     * @param array $config The client configurations.
     */
    public function __construct(array $config)
    {
        $this->clients = array();
        $this->methodMap = $config['methods'];
    }

    /**
     * Initialize OAuth2 credentials for API methods requiring them.
     *
     * @param string $login The account email or user name.
     * @param string $password The account password.
     * @param string $agent The game related to the authentication.
     *
     * @return boolean
     */
    public function authenticate($login, $password, $agent = null)
    {
        try {
            $authClient = $this->getClientForMethod('authenticate');
            $authClient->authenticate($login, $password, $agent);
            $this->credentials = $authClient->getCredentials();
            return true;
        }
        catch (MojangApiException $e) {
            $this->lastError = $e->getMessage();
        }

        return false;
    }

    /**
     * Change a profile's skin by UUID.
     *
     * @param $uuid The UUID of the user changing skin.
     * @param $url The URL location of the skin file.
     * @param $model The skin model type.
     */
    public function changeSkin($uuid, $url, $model = GHTMojangApiClient::SKIN_MODEL_STANDARD)
    {
        try {
            $this->getClientForMethod('changeSkin')->changeSkin($uuid, $url, $model);
            return true;
        }
        catch (MojangApiException $e) {
            $this->lastError = $e->getMessage();
        }

        return false;
    }

    /**
     * Get the authenticated account UUID, if any.  Provides access to the data
     * returned by a successful authentication without exposing the token or
     * requiring a second API call to get user information.
     *
     * @return string
     */
    public function getAuthenticatedId()
    {
        return ($this->credentials instanceof Credentials)
            ? $this->credentials->getProfileId()
            : null
        ;
    }

    /**
     * Get the authenticated account name, if any.  Provides access to the data
     * returned by a successful authentication without exposing the token or
     * requiring a second API call to get user information.
     *
     * @return string
     */
    public function getAuthenticatedName()
    {
        return ($this->credentials instanceof Credentials)
            ? $this->credentials->getProfileName()
            : null
        ;
    }

    /**
     * Get the instance of the Mojang API Client responsible for handling the
     * intended method.
     *
     * @param string $method The intented client method.
     *
     * @return \GHT\MojangApiClient\GHTMojangApiClient
     */
    protected function getClientForMethod($method)
    {
        // Method must be mapped
        if (!isset($this->methodMap[$method])) {
            throw new \InvalidArgumentException(sprintf('Invalid Mojang API method "%s", no API target found.', $method));
        }

        // Check if this client has already been instantiated
        $clientName = $this->methodMap[$method]['domain'] . ($this->methodMap[$method]['oauth'] ? 'Oauth' : '');
        if (isset($this->clients[$clientName]) && $this->clients[$clientName] instanceof GHTMojangApiClient) {
            return $this->clients[$clientName];
        }

        // Check if needing credentials, and they haven't yet been set
        if ($this->methodMap[$method]['oauth'] && !($this->credentials instanceof Credentials)) {
            throw new \RuntimeException(sprintf('Mojang API method "%s" requires authentication.', $method));
        }

        // Instantiate the required client
        switch ($this->methodMap[$method]['domain']) {
            case GHTMojangApiClient::SUBDOMAIN_API:
                // Check if authenticated method
                if ($this->methodMap[$method]['oauth']) {
                    $this->clients[$clientName] = GHTMojangApiClient::createApiClient($this->credentials);
                }
                else {
                    $this->clients[$clientName] = GHTMojangApiClient::createApiClient();
                }
                break;
            case GHTMojangApiClient::SUBDOMAIN_OAUTH:
                $this->clients[$clientName] = GHTMojangApiClient::createAuthenticationClient();
                break;
            case GHTMojangApiClient::SUBDOMAIN_SESSION:
                $this->clients[$clientName] = GHTMojangApiClient::createSessionClient();
                break;
            case GHTMojangApiClient::SUBDOMAIN_STATUS:
                $this->clients[$clientName] = GHTMojangApiClient::createStatusClient();
                break;
        }

        return $this->clients[$clientName];
    }

    /**
     * Return the last error type and message, if any.
     *
     * @return string
     */
    public function getLastError()
    {
        return $this->lastError;
    }

    /**
     * Return the last error message, if any.
     *
     * @return string
     */
    public function getLastErrorMessage()
    {
        return $this->lastError ? preg_replace('/^[^:]*: */', '', $this->lastError) : null;
    }

    /**
     * Return the last error type, if any.
     *
     * @return string
     */
    public function getLastErrorType()
    {
        return $this->lastError ? preg_replace('/^([^:]*):.*$/', '\1', $this->lastError) : null;
    }

    /**
     * Get all names ever used by a profile.
     *
     * @param string $uuid The UUID of the profile.
     *
     * @return array
     */
    public function getNames($uuid)
    {
        return $this->getClientForMethod('getNames')
            ->getNames($uuid)
        ;
    }

    /**
     * Get profile data for a UUID.
     *
     * @param string $uuid The UUID of the profile.
     *
     * @return array
     */
    public function getProfile($uuid)
    {
        return $this->getClientForMethod('getProfile')
            ->getProfile($uuid)
        ;
    }

    /**
     * Get profile data for a user name at a given time.
     *
     * @param string $name The user name.
     * @param \DateTime $when The date and time.
     *
     * @return array
     */
    public function getProfileForName($name, \DateTime $when = null)
    {
        return $this->getClientForMethod('getProfileForName')
            ->getProfileForName($name, $when)
        ;
    }

    /**
     * Get profile data for user names.
     *
     * @param array $names The user names.
     *
     * @return array
     */
    public function getProfilesForNames(array $names)
    {
        return $this->getClientForMethod('getProfilesForNames')
            ->getProfilesForNames($names)
        ;
    }

    /**
     * Get the current Minecraft statistics.  Specify the metric key or an
     * array of metric keys to request only those statistics, otherwise all
     * available statistics are requested.
     *
     * By default, statistics are aggregated.  To request separate statistics
     * for each metric, set the $split flag to true.  WARNING: a separate API
     * request is made for each specified metric, affecting the rate at which
     * your application may reach the number of requests limit.  Result will be
     * an array of statistics arrays keyed by each metric key.
     *
     * @param string|array<string> $metrics The metric key or keys.
     * @param boolean $split Set to true to get split metrics, not aggregate.
     *
     * @return array
     */
    public function getStatistics($metrics = null, $split = null)
    {
        return $this->getClientForMethod('getStatistics')
            ->getStatistics($metrics, $split)
        ;
    }

    /**
     * Get the current status of Mojang services.
     *
     * @return array
     */
    public function getStatus()
    {
        return $this->getClientForMethod('getStatus')
            ->getStatus()
        ;
    }

    /**
     * Get information for the current user.
     *
     * @return array
     */
    public function getUserInformation()
    {
        return $this->getClientForMethod('getUserInformation')
            ->getUserInformation()
        ;
    }

    /**
     * Invalidate authentication token.
     */
    public function invalidate()
    {
        try {
            $this->getClientForMethod('invalidate')->invalidate();
            return true;
        }
        catch (MojangApiException $e) {
            $this->lastError = $e->getMessage();
        }

        return false;
    }

    /**
     * Check if a server is blocked.
     *
     * @param string $url Any URL containing a server host / domain name.
     *
     * @return array
     */
    public function isServerBlocked($url)
    {
        // Attempt to parse the URL
        $urlParsed = parse_url($url);

        if (!$urlParsed || empty($urlParsed['host'])) {
            throw new \InvalidArgumentException(sprintf('Invalid URL, cannot parse: %s', $url));
        }

        // Get the blocked server hashes
        $hashes = $this->getClientForMethod('getBlockedServerHashes')
            ->getBlockedServerHashes()
        ;

        // Check the host and subdomain
        if (in_array(sha1($urlParsed['host']), $hashes)) {
            return true;
        }

        // Check the host by ascending subdomain
        $hostParts = explode('.', $urlParsed['host']);
        while (count($hostParts) > 2) {
            $hostParts[0] = '*';
            if (in_array(sha1(implode('.', $hostParts)), $hashes)) {
                return true;
            }
            array_shift($hostParts);
        }

        return false;
    }

    /**
     * Refresh authentication token.
     *
     * @return array
     */
    public function refresh()
    {
        try {
            $this->getClientForMethod('refresh')->refresh();
            return true;
        }
        catch (MojangApiException $e) {
            $this->lastError = $e->getMessage();
        }

        return false;
    }

    /**
     * Reset a profile's skin by UUID.
     *
     * @param string $uuid The UUID of the profile.
     */
    public function resetSkin($uuid)
    {
        try {
            $this->getClientForMethod('resetSkin')->resetSkin($uuid);
            return true;
        }
        catch (MojangApiException $e) {
            $this->lastError = $e->getMessage();
        }

        return false;
    }

    /**
     * Set the MojangApiClient for an API target manually.  Allows the client to
     * be extended without needing to also extend this service.  Also useful for
     * unit testing.
     *
     * @param \GHT\MojangApiClient\GHTMojangApiClient $client The client.
     * @param string $domain The target API domain.
     * @param boolean $oauth Set to true if client requires authentication.
     *
     * @return GHTApiClientService
     */
    public function setClient(GHTMojangApiClient $client, $domain, $oauth = false)
    {
        // Validate the API domain name
        if (!in_array($domain, GHTMojangApiClient::getApiNames())) {
            throw new \InvalidArgumentException(sprintf(
                '"%s" is not a valid API subdomain name.  Expected one of: %s',
                $domain,
                implode(', ', GHTMojangApiClient::getApiNames())
            ));
        }

        // Set the proper client
        $clientName = $domain . (($domain === GHTMojangApiClient::SUBDOMAIN_API && $oauth) ? 'Oauth' : '');
        $this->clients[$clientName] = $client;

        return $this;
    }

    /**
     * Invalidate authentication token using login name and password.
     *
     * @param string $login The account email or user name.
     * @param string $paasword The account password.
     */
    public function signOut($login, $password)
    {
        try {
            $this->getClientForMethod('signOut')->signOut($login, $password);
            return true;
        }
        catch (MojangApiException $e) {
            $this->lastError = $e->getMessage();
        }

        return false;
    }

    /**
     * Upload a new skin for a profile by UUID.
     *
     * @param string $uuid The UUID of the profile.
     * @param string $pathName The path and name of the skin file.
     * @param $model The skin model type.
     */
    public function uploadSkin($uuid, $pathName, $model = GHTMojangApiClient::SKIN_MODEL_STANDARD)
    {
        try {
            $this->getClientForMethod('uploadSkin')->uploadSkin($uuid, $pathName, $model);
            return true;
        }
        catch (MojangApiException $e) {
            $this->lastError = $e->getMessage();
        }

        return false;
    }

    /**
     * Validate authentication token.
     */
    public function validate()
    {
        try {
            $this->getClientForMethod('validate')->validate();
            return true;
        }
        catch (MojangApiException $e) {
            $this->lastError = $e->getMessage();
        }

        return false;
   }
}