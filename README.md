GHT Mojang API Client Bundle
=========================

This bundle provides the [GHT Mojang API Client](https://bitbucket.org/greenhollowtech/ght-mojang-api-client) as a service in Symfony.

# Installation

## Get the Composer package

To install with [Composer](https://getcomposer.org/), run `composer require greenhollowtech/ght-mojang-api-client-bundle`.

## Add the GHTMojangApiClientBundle to your Symfony application

```php
// app/AppKernel.php

    public function registerBundles()
    {
        return array(
            // ...
            new GHT\MojangApiClientBundle\GHTMojangApiClientBundle(),
            // ...
        );
    }
```

# Usage

## Unauthenticated API Methods

```php
$mojangClient = $this->get('ght_mojang_api_client');

// Get all names a user has ever used
$names = $mojangClient->getNames($uuid);

// Get profile data for a UUID
$profile = $mojangClient->getProfile($uuid);

// Get the UUID for the user currently or in the past using a given name
$profile = $mojangClient->getProfileForName('SomeDude');
$profile = $mojangClient->getProfileForName('SomeDude', new \DateTime('-1 month'));

// Get the UUIDs (and possibly other limited information) of a list of names
$profiles = $mojangClient->getProfilesForNames(array('SomeDude', 'SomeOtherDude'));

// Get Mojang statistics
$statistics = $mojangClient->getStatistics();
$metrics = array('item_sold_minecraft', 'item_sold_scrolls');
$statistics = $mojangClient->getStatistics($metrics);
$splitByMetric = true;
$statistics = $mojangClient->getStatistics($metrics, $splitByMetric);

// Get statuses for all services
$statuses = $mojangClient->getStatus();
$minecraftStatus = $statuses['minecraft.net'];

// Check a URL if the server is blocked
$serverIsBlocked = $mojangClient->isServerBlocked('https://some.test.myservername.com');
```

## Authentication Methods

All of these methods will return true or false.  If failing, the captured error message can be accessed.

```php
// Authenticate the client
if (!$mojangClient->authenticate($email, $password)) {
    // authentication failed
    echo $mojangClient->getLastError();
}

// Validate the current credentials
if (!$mojangClient->validate()) {
    // authentication is invalid
    echo $mojangClient->getLastError();
}

// Authenticating with a game as the agent provides better credentials
$mojangClient->authenticate($email, $password, 'Minecraft');

// Refreshes the API token transported in the client
$mojangClient->refresh();

// End the authenticated session by invalidating the token
$mojangClient->invalidate();

// End the authenticated session using the login name and password
$mojangClient->signOut($email, $password);
```

## Authenticated API Methods

All of these methods require the client to be authenticated first.

```php
// Authenticate the client
if (!$mojangClient->authenticate($email, $password, 'Minecraft')) {
    if (!$mojangClient->authenticate($email, $password)) {
        // authentication failed, do something!
    }
}

// Change a skin
if (!$mojangClient->changeSkin($uuid, $url, 'slim')) {
    echo sprintf('Could not change skin! (%s)', $mojangClient->getLastError());
}

// Get the current user's information
$userInfo = $mojangClient->getUserInformation();

// Reset the user's skin
if (!$mojangClient->resetSkin($uuid)) {
    echo sprintf('Could not reset skin! (%s)', $mojangClient->getLastError());
}

// Upload a skin
if (!$mojangClient->uploadSkin($uuid, '/tmp/skinFile.png', 'slim')) {
    echo sprintf('Could not upload skin! (%s)', $mojangClient->getLastError());
}
```
