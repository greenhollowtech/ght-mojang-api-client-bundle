<?php

namespace GHT\MojangApiClientBundle\Tests\DependencyInjection;

use GHT\MojangApiClientBundle\DependencyInjection\GHTMojangApiClientExtension;
use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;

/**
 * Exercises the dependency injection on a compiler pass of the
 * GHTMojangApiClientBundle.
 */
class GHTMojangApiClientExtensionTest extends AbstractExtensionTestCase
{
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * {@inheritdoc}
     */
    protected function getContainerExtensions()
    {
        return array(
            new GHTMojangApiClientExtension(),
        );
    }

    /**
     * Verify that the API method map is loaded.
     */
    public function testParametersContainApiMethodMap()
    {
        $this->load();

        $this->assertContainerBuilderHasParameter('ght_mojang_api_client.methods.authenticate', array('domain' => 'authserver', 'oauth' => false));
        $this->assertContainerBuilderHasParameter('ght_mojang_api_client.methods.changeSkin', array('domain' => 'api', 'oauth' => true));
        $this->assertContainerBuilderHasParameter('ght_mojang_api_client.methods.getBlockedServerHashes', array('domain' => 'sessionserver', 'oauth' => false));
        $this->assertContainerBuilderHasParameter('ght_mojang_api_client.methods.getNames', array('domain' => 'api', 'oauth' => false));
        $this->assertContainerBuilderHasParameter('ght_mojang_api_client.methods.getProfile', array('domain' => 'sessionserver', 'oauth' => false));
        $this->assertContainerBuilderHasParameter('ght_mojang_api_client.methods.getProfileForName', array('domain' => 'api', 'oauth' => false));
        $this->assertContainerBuilderHasParameter('ght_mojang_api_client.methods.getProfilesForNames', array('domain' => 'api', 'oauth' => false));
        $this->assertContainerBuilderHasParameter('ght_mojang_api_client.methods.getStatistics', array('domain' => 'api', 'oauth' => false));
        $this->assertContainerBuilderHasParameter('ght_mojang_api_client.methods.getStatus', array('domain' => 'status', 'oauth' => false));
        $this->assertContainerBuilderHasParameter('ght_mojang_api_client.methods.getUserInformation', array('domain' => 'api', 'oauth' => true));
        $this->assertContainerBuilderHasParameter('ght_mojang_api_client.methods.invalidate', array('domain' => 'authserver', 'oauth' => false));
        $this->assertContainerBuilderHasParameter('ght_mojang_api_client.methods.refresh', array('domain' => 'authserver', 'oauth' => false));
        $this->assertContainerBuilderHasParameter('ght_mojang_api_client.methods.resetSkin', array('domain' => 'api', 'oauth' => true));
        $this->assertContainerBuilderHasParameter('ght_mojang_api_client.methods.signOut', array('domain' => 'authserver', 'oauth' => false));
        $this->assertContainerBuilderHasParameter('ght_mojang_api_client.methods.uploadSkin', array('domain' => 'api', 'oauth' => true));
        $this->assertContainerBuilderHasParameter('ght_mojang_api_client.methods.validate', array('domain' => 'authserver', 'oauth' => false));
    }

    /**
     * Verify that the service is loaded.
     */
    public function testMojangApiClientServiceIsLoaded()
    {
        $this->load();

        $this->assertContainerBuilderHasService('ght_mojang_api_client', 'GHT\MojangApiClientBundle\Service\GHTMojangApiClientService');
    }
}