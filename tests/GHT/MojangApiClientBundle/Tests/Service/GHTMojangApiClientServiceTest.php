<?php

namespace GHT\MojangApiClientBundle\Tests\Service;

use GHT\MojangApiClient\Authentication\Credentials;
use GHT\MojangApiClient\Exception\MojangApiException;
use GHT\MojangApiClientBundle\Service\GHTMojangApiClientService;

/**
 * Exercises the GHTMojangApiClientService.
 */
class GHTMojangApiClientServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * \GHT\MojangApiClientBundle\Service\GHTMojangApiClientService
     */
    protected $mojangApiClientService;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        // Instantiate the service with basic mocked clients for all tests
        $this->mojangApiClientService = new GHTMojangApiClientService($this->configureParameters());

        $this->mojangApiClientService->setClient($this->configureMojangApiClient(), 'api')
            ->setClient($this->configureMojangApiClient(), 'api', true)
            ->setClient($this->configureMojangApiClient(), 'authserver')
            ->setClient($this->configureMojangApiClient(), 'sessionserver')
            ->setClient($this->configureMojangApiClient(), 'status')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        unset($this->mojangApiClientService);
    }

    /**
     * Get a mocked Mojang API client.
     *
     * @return \GHT\MojangApiClient\GHTMojangApiClient
     */
    protected function configureMojangApiClient()
    {
        return $this->getMockBuilder('\GHT\MojangApiClient\GHTMojangApiClient')
            ->disableOriginalConstructor()
            ->setMethods(array(
                'authenticate',
                'changeSkin',
                'getBlockedServerHashes',
                'getCredentials',
                'getNames',
                'getProfile',
                'getProfileForName',
                'getProfilesForNames',
                'getStatistics',
                'getStatus',
                'getUserInformation',
                'invalidate',
                'refresh',
                'resetSkin',
                'signOut',
                'uploadSkin',
                'validate',
            ))
            ->getMock()
        ;
    }

    /**
     * Get the expected configuration array.
     *
     * @return array
     */
    protected function configureParameters()
    {
        return array(
            'methods' => array(
                'authenticate' => array('domain' => 'authserver', 'oauth' => false),
                'changeSkin' => array('domain' => 'api', 'oauth' => true),
                'getBlockedServerHashes' => array('domain' => 'sessionserver', 'oauth' => false),
                'getNames' => array('domain' => 'api', 'oauth' => false),
                'getProfile' => array('domain' => 'sessionserver', 'oauth' => false),
                'getProfileForName' => array('domain' => 'api', 'oauth' => false),
                'getProfilesForNames' => array('domain' => 'api', 'oauth' => false),
                'getStatistics' => array('domain' => 'api', 'oauth' => false),
                'getStatus' => array('domain' => 'status', 'oauth' => false),
                'getUserInformation' => array('domain' => 'api', 'oauth' => true),
                'invalidate' => array('domain' => 'authserver', 'oauth' => false),
                'refresh' => array('domain' => 'authserver', 'oauth' => false),
                'resetSkin' => array('domain' => 'api', 'oauth' => true),
                'signOut' => array('domain' => 'authserver', 'oauth' => false),
                'uploadSkin' => array('domain' => 'api', 'oauth' => true),
                'validate' => array('domain' => 'authserver', 'oauth' => false),
            ),
        );
    }

    /**
     * Verify that authentication can be requested.
     */
    public function testAuthenticate()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())->method('authenticate');
        $client->expects($this->once())->method('getCredentials');
        $this->mojangApiClientService->setClient($client, 'authserver');

        // Verify that no exception is thrown
        $this->assertTrue($this->mojangApiClientService->authenticate('test@email.com', 'testPassword', 'Minecraft'));
    }

    /**
     * Verify that an authentication error can be captured.
     */
    public function testAuthenticateFailure()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('authenticate')
            ->will($this->throwException(
                new MojangApiException('{"error":"ForbiddenOperationException","errorMessage":"Invalid credentials. Invalid username or password."}')
            ))
        ;
        $client->expects($this->never())->method('getCredentials');
        $this->mojangApiClientService->setClient($client, 'authserver');

        // Verify that the thrown exception is captured
        $this->assertFalse($this->mojangApiClientService->authenticate('test@email.com', 'testPassword', 'Minecraft'));
        $this->assertEquals(
            'ForbiddenOperationException: Invalid credentials. Invalid username or password.',
            $this->mojangApiClientService->getLastError()
        );
    }

    /**
     * Verify that a skin change can be requested.
     */
    public function testChangeSkin()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('changeSkin')
            ->with(
                $this->equalTo('testUuid'),
                $this->equalTo('https://test.greenhollowtech.com/skins/test.png')
            )
        ;
        $this->mojangApiClientService->setClient($client, 'api', true);

        // Verify that no exception is thrown
        $this->assertTrue($this->mojangApiClientService->changeSkin('testUuid', 'https://test.greenhollowtech.com/skins/test.png'));
    }

    /**
     * Verify that an error thrown on a skin change request can be captured.
     */
    public function testChangeSkinFailure()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('changeSkin')
            ->will($this->throwException(
                new MojangApiException('{"error":"ForbiddenOperationException","errorMessage":"Invalid token."}')
            ))
        ;
        $this->mojangApiClientService->setClient($client, 'api', true);

        // Verify that the thrown exception is captured
        $this->assertFalse($this->mojangApiClientService->changeSkin('testUuid', 'https://test.greenhollowtech.com/skins/test.png'));
        $this->assertEquals(
            'ForbiddenOperationException: Invalid token.',
            $this->mojangApiClientService->getLastError()
        );
    }

    /**
     * Verify that the authenticated user UUID can be requested.
     */
    public function testGetAuthenticatedId()
    {
        // Set up the client expectations
        $credentials = new Credentials();
        $credentials->setProfileId('testUuid');

        $client = $this->configureMojangApiClient();
        $client->expects($this->once())->method('authenticate');
        $client->expects($this->once())
            ->method('getCredentials')
            ->will($this->returnValue($credentials))
        ;
        $this->mojangApiClientService->setClient($client, 'authserver');

        // Authenticate the user
        $this->mojangApiClientService->authenticate('test@email.com', 'testPassword', 'Minecraft');

        // Verify the UUID from the credentials is returned
        $this->assertEquals('testUuid', $this->mojangApiClientService->getAuthenticatedId());
    }

    /**
     * Verify that the authenticated user UUID can be requested without error
     * when not authenticated.
     */
    public function testGetAuthenticatedIdWhenNotAuthenticated()
    {
        // Verify nothing is returned when the credentials don't exist
        $this->assertNull($this->mojangApiClientService->getAuthenticatedId());
    }

    /**
     * Verify that the authenticated user name can be requested.
     */
    public function testGetAuthenticatedName()
    {
        // Set up the client expectations
        $credentials = new Credentials();
        $credentials->setProfileName('testName');

        $client = $this->configureMojangApiClient();
        $client->expects($this->once())->method('authenticate');
        $client->expects($this->once())
            ->method('getCredentials')
            ->will($this->returnValue($credentials))
        ;
        $this->mojangApiClientService->setClient($client, 'authserver');

        // Authenticate the user
        $this->mojangApiClientService->authenticate('test@email.com', 'testPassword', 'Minecraft');

        // Verify the UUID from the credentials is returned
        $this->assertEquals('testName', $this->mojangApiClientService->getAuthenticatedName());
    }

    /**
     * Verify that the authenticated user name can be requested without error
     * when not authenticated.
     */
    public function testGetAuthenticatedNameWhenNotAuthenticated()
    {
        // Verify nothing is returned when the credentials don't exist
        $this->assertNull($this->mojangApiClientService->getAuthenticatedName());
    }

    /**
     * Verify that the last error message can be parsed.
     */
    public function testGetLastErrorMessage()
    {
        $reflectionClass = new \ReflectionClass($this->mojangApiClientService);
        $property = $reflectionClass->getProperty('lastError');
        $property->setAccessible(true);
        $property->setValue($this->mojangApiClientService, 'TestException: Test error message.');

        $this->assertEquals('Test error message.', $this->mojangApiClientService->getLastErrorMessage());
    }

    /**
     * Verify that the last error message parsing on a null value returns null.
     */
    public function testGetLastErrorMessageWhenNull()
    {
        $this->assertNull($this->mojangApiClientService->getLastErrorMessage());
    }

    /**
     * Verify that the last error type can be parsed.
     */
    public function testGetLastErrorType()
    {
        $reflectionClass = new \ReflectionClass($this->mojangApiClientService);
        $property = $reflectionClass->getProperty('lastError');
        $property->setAccessible(true);
        $property->setValue($this->mojangApiClientService, 'TestException: Test error message.');

        $this->assertEquals('TestException', $this->mojangApiClientService->getLastErrorType());
    }

    /**
     * Verify that the last error type parsing on a null value returns null.
     */
    public function testGetLastErrorTypeWhenNull()
    {
        $this->assertNull($this->mojangApiClientService->getLastErrorType());
    }

    /**
     * Verify that names can be requested by UUID.
     */
    public function testGetNames()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('getNames')
            ->with($this->equalTo('testUuid'))
            ->will($this->returnValue(array(array('name' => 'testName'))))
        ;
        $this->mojangApiClientService->setClient($client, 'api');

        // Verify that no exception is thrown
        $this->assertEquals(
            array(array('name' => 'testName')),
            $this->mojangApiClientService->getNames('testUuid')
        );
    }

    /**
     * Verify that a profile can be requested by UUID.
     */
    public function testGetProfile()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('getProfile')
            ->with($this->equalTo('testUuid'))
            ->will($this->returnValue(array(
                'id' => 'testUuid',
                'name' => 'testName',
                'properties' => array(),
            )))
        ;
        $this->mojangApiClientService->setClient($client, 'sessionserver');

        // Verify that no exception is thrown
        $this->assertEquals(
            array('id' => 'testUuid', 'name' => 'testName', 'properties' => array()),
            $this->mojangApiClientService->getProfile('testUuid')
        );
    }

    /**
     * Verify that a profile can be requested by name.
     */
    public function testGetProfileForName()
    {
        // Set up the client expectations
        $lastMonth = new \DateTime('-1 month');
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('getProfileForName')
            ->with(
                $this->equalTo('testName'),
                $this->equalTo($lastMonth)
            )
            ->will($this->returnValue(array('id' => 'testUuid', 'name' => 'testName')))
        ;
        $this->mojangApiClientService->setClient($client, 'api');

        // Verify that no exception is thrown
        $this->assertEquals(
            array('id' => 'testUuid', 'name' => 'testName'),
            $this->mojangApiClientService->getProfileForName('testName', $lastMonth)
        );
    }

    /**
     * Verify that multiple profiles can be requested by names.
     */
    public function testGetProfilesForNames()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('getProfilesForNames')
            ->with($this->equalTo(array('testName1', 'testName2')))
            ->will($this->returnValue(array(
                array('id' => 'testUuid1', 'name' => 'testName1'),
                array('id' => 'testUuid2', 'name' => 'testName2'),
            )))
        ;
        $this->mojangApiClientService->setClient($client, 'api');

        // Verify that no exception is thrown
        $this->assertEquals(
            array(
                array('id' => 'testUuid1', 'name' => 'testName1'),
                array('id' => 'testUuid2', 'name' => 'testName2'),
            ),
            $this->mojangApiClientService->getProfilesForNames(array('testName1', 'testName2'))
        );
    }

    /**
     * Verify that statistics can be requested.
     */
    public function testGetStatistics()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('getStatistics')
            ->with($this->equalTo(array('item_sold_minecraft')))
            ->will($this->returnValue(array(
                'total' => '12345678',
                'last24h' => '6789',
                'saleVelocityPerSeconds' => '0.098765432',
            )))
        ;
        $this->mojangApiClientService->setClient($client, 'api');

        // Verify that no exception is thrown
        $this->assertEquals(
            array('total' => '12345678', 'last24h' => '6789', 'saleVelocityPerSeconds' => '0.098765432'),
            $this->mojangApiClientService->getStatistics(array('item_sold_minecraft'))
        );
    }

    /**
     * Verify that the statuses of Mojang services can be requested.
     */
    public function testGetStatus()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('getStatus')
            ->will($this->returnValue(array(
                'minecraft.net' => 'yellow',
                'session.minecraft.net' => 'green',
            )))
        ;
        $this->mojangApiClientService->setClient($client, 'status');

        // Verify that no exception is thrown
        $this->assertEquals(
            array('minecraft.net' => 'yellow', 'session.minecraft.net' => 'green'),
            $this->mojangApiClientService->getStatus()
        );
    }

    /**
     * Verify that user information can be requested.
     */
    public function testGetUserInformation()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('getUserInformation')
            ->will($this->returnValue(array('id' => 'testUuid', 'email' => 'test@email.com')))
        ;
        $this->mojangApiClientService->setClient($client, 'api', true);

        // Verify that no exception is thrown
        $this->assertEquals(
            array('id' => 'testUuid', 'email' => 'test@email.com'),
            $this->mojangApiClientService->getUserInformation()
        );
    }

    /**
     * Verify that authentication can be invalidated on request.
     */
    public function testInvalidate()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())->method('invalidate');
        $this->mojangApiClientService->setClient($client, 'authserver');

        // Verify that no exception is thrown
        $this->assertTrue($this->mojangApiClientService->invalidate());
    }

    /**
     * Verify that an error thrown attempting to invalidate can be captured.
     */
    public function testInvalidateFailure()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('invalidate')
            ->will($this->throwException(
                new MojangApiException('{"error":"ForbiddenOperationException","errorMessage":"Invalid token."}')
            ))
        ;
        $this->mojangApiClientService->setClient($client, 'authserver');

        // Verify that the thrown exception is captured
        $this->assertFalse($this->mojangApiClientService->invalidate());
        $this->assertEquals(
            'ForbiddenOperationException: Invalid token.',
            $this->mojangApiClientService->getLastError()
        );
    }

    /**
     * Verify that a server can detect if it is being blocked.
     */
    public function testIsServerBlockedWhenBlocked()
    {
        // Create some test hashes
        $hashes = array(
            sha1('*.testone.com'),
            sha1('*.testtwo.com'),
            sha1('*.testthree.net'),
        );

        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('getBlockedServerHashes')
            ->will($this->returnValue($hashes))
        ;
        $this->mojangApiClientService->setClient($client, 'sessionserver');

        // Verify that no exception is thrown
        $this->assertTrue($this->mojangApiClientService->isServerBlocked('https://subdomain.testtwo.com/some/path'));
    }

    /**
     * Verify that a server can detect if it is not being blocked.
     */
    public function testIsServerBlockedWhenNotBlocked()
    {
        // Create some test hashes
        $hashes = array(
            sha1('*.testone.com'),
            sha1('*.testtwo.com'),
            sha1('*.testthree.net'),
        );

        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('getBlockedServerHashes')
            ->will($this->returnValue($hashes))
        ;
        $this->mojangApiClientService->setClient($client, 'sessionserver');

        // Verify that no exception is thrown
        $this->assertFalse($this->mojangApiClientService->isServerBlocked('https://subdomain.notblocked.com/some/path'));
    }

    /**
     * Verify that checking if a server is blocked using a malformed URL throws
     * an exception.
     */
    public function testIsServerBlockedInvalidUrl()
    {
        // Expect the exception
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid URL, cannot parse: /some/path');

        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->never())->method('getBlockedServerHashes');
        $this->mojangApiClientService->setClient($client, 'sessionserver');

        // Trigger the exception
        $this->assertFalse($this->mojangApiClientService->isServerBlocked('/some/path'));
    }

    /**
     * Verify that authentication can be refreshed on request.
     */
    public function testRefresh()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())->method('refresh');
        $this->mojangApiClientService->setClient($client, 'authserver');

        // Verify that no exception is thrown
        $this->assertTrue($this->mojangApiClientService->refresh());
    }

    /**
     * Verify that an error thrown attempting to refresh can be captured.
     */
    public function testRefreshFailure()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('refresh')
            ->will($this->throwException(
                new MojangApiException('{"error":"ForbiddenOperationException","errorMessage":"Invalid token."}')
            ))
        ;
        $this->mojangApiClientService->setClient($client, 'authserver');

        // Verify that the thrown exception is captured
        $this->assertFalse($this->mojangApiClientService->refresh());
        $this->assertEquals(
            'ForbiddenOperationException: Invalid token.',
            $this->mojangApiClientService->getLastError()
        );
    }

    /**
     * Verify that a skin reset can be requested.
     */
    public function testResetSkin()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('resetSkin')
            ->with($this->equalTo('testUuid'))
        ;
        $this->mojangApiClientService->setClient($client, 'api', true);

        // Verify that no exception is thrown
        $this->assertTrue($this->mojangApiClientService->resetSkin('testUuid'));
    }

    /**
     * Verify that an error thrown on a skin reset request can be captured.
     */
    public function testResetSkinFailure()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('resetSkin')
            ->will($this->throwException(
                new MojangApiException('{"error":"ForbiddenOperationException","errorMessage":"Invalid token."}')
            ))
        ;
        $this->mojangApiClientService->setClient($client, 'api', true);

        // Verify that the thrown exception is captured
        $this->assertFalse($this->mojangApiClientService->resetSkin('testUuid'));
        $this->assertEquals(
            'ForbiddenOperationException: Invalid token.',
            $this->mojangApiClientService->getLastError()
        );
    }

    /**
     * Verify that authentication can be ended by signing out.
     */
    public function testSignOut()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('signOut')
            ->with(
                $this->equalTo('testName'),
                $this->equalTo('testPassword')
            )
        ;
        $this->mojangApiClientService->setClient($client, 'authserver');

        // Verify that no exception is thrown
        $this->assertTrue($this->mojangApiClientService->signOut('testName', 'testPassword'));
    }

    /**
     * Verify that an error thrown attempting to sign out can be captured.
     */
    public function testSignOutFailure()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('signOut')
            ->will($this->throwException(
                new MojangApiException('{"error":"ForbiddenOperationException","errorMessage":"Invalid token."}')
            ))
        ;
        $this->mojangApiClientService->setClient($client, 'authserver');

        // Verify that the thrown exception is captured
        $this->assertFalse($this->mojangApiClientService->signOut('testName', 'testPassword'));
        $this->assertEquals(
            'ForbiddenOperationException: Invalid token.',
            $this->mojangApiClientService->getLastError()
        );
    }

    /**
     * Verify that a skin upload can be requested.
     */
    public function testUploadSkin()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('uploadSkin')
            ->with(
                $this->equalTo('testUuid'),
                $this->equalTo('/tmp/test.png'),
                $this->equalTo('standard')
            )
        ;
        $this->mojangApiClientService->setClient($client, 'api', true);

        // Verify that no exception is thrown
        $this->assertTrue($this->mojangApiClientService->uploadSkin('testUuid', '/tmp/test.png'));
    }

    /**
     * Verify that an error thrown on a skin upload request can be captured.
     */
    public function testUploadSkinFailure()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('uploadSkin')
            ->will($this->throwException(
                new MojangApiException('{"error":"ForbiddenOperationException","errorMessage":"Invalid token."}')
            ))
        ;
        $this->mojangApiClientService->setClient($client, 'api', true);

        // Verify that the thrown exception is captured
        $this->assertFalse($this->mojangApiClientService->uploadSkin('testUuid', '/tmp/test.png'));
        $this->assertEquals(
            'ForbiddenOperationException: Invalid token.',
            $this->mojangApiClientService->getLastError()
        );
    }

    /**
     * Verify that authentication can be validated on request.
     */
    public function testValidate()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())->method('validate');
        $this->mojangApiClientService->setClient($client, 'authserver');

        // Verify that no exception is thrown
        $this->assertTrue($this->mojangApiClientService->validate());
    }

    /**
     * Verify that an error thrown attempting to validate can be captured.
     */
    public function testValidateFailure()
    {
        // Set up the client expectations
        $client = $this->configureMojangApiClient();
        $client->expects($this->once())
            ->method('validate')
            ->will($this->throwException(
                new MojangApiException('{"error":"ForbiddenOperationException","errorMessage":"Invalid token."}')
            ))
        ;
        $this->mojangApiClientService->setClient($client, 'authserver');

        // Verify that the thrown exception is captured
        $this->assertFalse($this->mojangApiClientService->validate());
        $this->assertEquals(
            'ForbiddenOperationException: Invalid token.',
            $this->mojangApiClientService->getLastError()
        );
    }
}
